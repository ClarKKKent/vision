<?php


namespace App\Infrastructure\User\Persistence\Query\Mysql;


use App\Infrastructure\Core\Exception\ModelNotFoundException;
use App\Infrastructure\Core\Persistence\Mysql\Query\AbstractMysqlCollection;
use App\Infrastructure\User\Persistence\Query\UserCollectionInterface;
use App\Infrastructure\User\Persistence\ReadModel\AuthView;
use App\Infrastructure\User\Persistence\ReadModel\UserView;
use Symfony\Component\Security\Core\User\UserInterface;


final class MysqlUserCollection extends AbstractMysqlCollection implements UserCollectionInterface
{
    public function oneByEmailForAuth(string $email): AuthView
    {
        $qb = $this->entityManager->getConnection()->createQueryBuilder()
            ->select('u.id', 'u.email', 'u.password_hash')
            ->from('user_users', 'u')
            ->where('email = :email')
            ->setParameter(':email', $email);

        $query = $this->entityManager->getConnection()->executeQuery($qb->getSql(), $qb->getParameters());
        $query->setFetchMode(\PDO::FETCH_CLASS, AuthView::class);
        $result = $query->fetch();

        if (!$result) {
            throw new ModelNotFoundException('User not found.');
        }

        return $result;
    }

    public function oneByIdentity(UserInterface $identity): UserView
    {
        $qb = $this->entityManager->getConnection()->createQueryBuilder()
            ->select('*')
            ->from('user_users', 'u')
            ->where('email = :email')
            ->setParameter(':email', $identity->getUsername());

        $query = $this->entityManager->getConnection()->executeQuery($qb->getSQL(), $qb->getParameters());
        $query->setFetchMode(\PDO::FETCH_CLASS, UserView::class);
        $result = $query->fetch();

        if (!$result) {
            throw new ModelNotFoundException('User not found.');
        }

        return $result;
    }
}