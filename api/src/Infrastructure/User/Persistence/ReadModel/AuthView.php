<?php


namespace App\Infrastructure\User\Persistence\ReadModel;


use App\Infrastructure\Core\Persistence\ReadModelInterface;

final class AuthView implements ReadModelInterface
{
    public ?string $id;

    public ?string $email;

    public ?string $password_hash;

    public function serialize(): array
    {
        return [];
    }
}