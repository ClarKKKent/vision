<?php


namespace App\Infrastructure\Core\Exception;


use Throwable;

final class ModelNotFoundException extends \LogicException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}