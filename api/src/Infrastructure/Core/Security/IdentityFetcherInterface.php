<?php


namespace App\Infrastructure\Core\Security;


use Symfony\Component\Security\Core\User\UserInterface;

interface IdentityFetcherInterface
{
    public function get(): UserInterface;
}