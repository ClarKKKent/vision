<?php


namespace App\Infrastructure\Core\Security\Password;


interface PasswordHasherInterface
{
    public function hash(string $password): string;

    public function verify(string $password, string $passwordHash): bool;
}