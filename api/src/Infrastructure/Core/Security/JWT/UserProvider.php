<?php


namespace App\Infrastructure\Core\Security\JWT;


use App\Infrastructure\Core\Security\UserIdentityFactoryInterface;
use App\Infrastructure\User\Persistence\Query\UserCollectionInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

final class UserProvider implements UserProviderInterface
{
    private UserCollectionInterface $userCollection;

    private UserIdentityFactoryInterface $userIdentityFactory;

    public function __construct(
        UserCollectionInterface $userCollection,
        UserIdentityFactoryInterface $userIdentityFactory)
    {
        $this->userCollection = $userCollection;
        $this->userIdentityFactory = $userIdentityFactory;
    }

    public function loadUserByUsername(string $email): UserInterface
    {
        $user = $this->userCollection->oneByEmailForAuth($email);

        return $this->userIdentityFactory->createFromUser($user);
    }

    public function refreshUser(UserInterface $user)
    {
        // TODO: Implement refreshUser() method.
    }

    public function supportsClass(string $class)
    {
        // TODO: Implement supportsClass() method.
    }
}