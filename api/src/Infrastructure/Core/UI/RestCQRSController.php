<?php

namespace App\Infrastructure\Core\UI;


use App\Application\Core\CommandInterface;
use App\Application\Core\QueryInterface;
use App\Infrastructure\Core\Bus\CommandBusInterface;
use App\Infrastructure\Core\Bus\QueryBusInterface;

class RestCQRSController extends RestController
{
    private CommandBusInterface $commandBus;

    private QueryBusInterface $queryBus;

    public function __construct(
        CommandBusInterface $commandBus,
        QueryBusInterface $queryBus,
        JsonApiFormatter $formatter)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
        parent::__construct($formatter);
    }

    protected function exec(CommandInterface $command): void
    {
        $this->commandBus->handle($command);
    }

    protected function ask(QueryInterface $query)
    {
        return $this->queryBus->handle($query);
    }
}