<?php


namespace App\Infrastructure\Core\UI;


use App\Application\Core\Collection;
use App\Application\Core\Item;
use App\Application\Core\PaginatedCollection;
use Symfony\Component\Routing\RouterInterface;

final class JsonApiFormatter
{
    private const FIRST_PAGE_INT = 1;

    private RouterInterface $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function one(Item $resource): array
    {
        return $resource->resource();
    }

    public function collection(Collection $collection): array
    {
        $transformer = function ($data) {
            return $data instanceof Item ? $data->resource() : $data;
        };

        return array_map($transformer, $collection->resource());
    }

    public function paginatedCollection(PaginatedCollection $collection): array
    {
        return [
            'items' => $collection->resource()->getItems(),
            'links' => [
                'first' => $this->getUrlWithQueryPattern() . self::FIRST_PAGE_INT,
                'prev' => $this->getUrlWithQueryPattern() . max($collection->resource()->getCurrentPageNumber() - 1, self::FIRST_PAGE_INT),
                'current' => $this->getUrlWithQueryPattern() . $collection->resource()->getCurrentPageNumber(),
                'next' => $this->getUrlWithQueryPattern() . min($collection->resource()->getCurrentPageNumber() + 1, ceil($collection->resource()->getTotalItemCount() / $collection->resource()->getItemNumberPerPage())),
                'last' => $this->getUrlWithQueryPattern() . ceil($collection->resource()->getTotalItemCount() / $collection->resource()->getItemNumberPerPage())
            ],
            'meta' => [
                'total_count' => $collection->resource()->getTotalItemCount(),
                'page_count' => ceil($collection->resource()->getTotalItemCount() / $collection->resource()->getItemNumberPerPage()),
                'current_page' => $collection->resource()->getCurrentPageNumber(),
                'per_page' => $collection->resource()->getItemNumberPerPage()
            ]
        ];
    }

    private function getUrl(): string
    {
        $urlContext = $this->router->getContext();

        return $urlContext->getScheme() . '://' . $urlContext->getHost() . $urlContext->getPathInfo();
    }

    private function getUrlWithQueryPattern(): string
    {
        return $this->getUrl() . '?page=';
    }
}