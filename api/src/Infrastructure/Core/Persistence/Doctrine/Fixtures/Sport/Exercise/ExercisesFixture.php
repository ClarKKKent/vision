<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise;


use App\Domain\Core\Uuid;
use App\Domain\Sport\Entity\Exercise;
use App\Infrastructure\Core\Persistence\Doctrine\Builders\User\User\UserBuilder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

final class ExercisesFixture extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $user = UserBuilder::any();

        for ($i = 1; $i <=3; $i++) {
            $exercise = Exercise::create(
                Uuid::fromString(\Ramsey\Uuid\Uuid::uuid4()),
                'Exercise ' . $i,
                'description'
            );
            $user->attachExercise($exercise);
        }

        $manager->persist($user);
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['group1'];
    }
}