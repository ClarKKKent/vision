<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User;


use App\Domain\Core\Uuid;
use App\Domain\User\Entity\User;
use App\Domain\User\ValueObject\Email;
use App\Infrastructure\Core\Persistence\Doctrine\Builders\User\User\UserBuilder;
use App\Infrastructure\Core\Security\Password\Bcrypt\BcryptPasswordHasher;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class UserForAuthFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = UserBuilder::any();

        $manager->persist($user);
        $manager->flush();
    }
}