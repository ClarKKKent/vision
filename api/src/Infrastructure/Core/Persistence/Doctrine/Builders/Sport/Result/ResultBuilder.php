<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Result;


use App\Domain\Core\Uuid;
use App\Domain\Sport\Entity\Result;

class ResultBuilder
{
    public const RESULT_TEST_ID = 'fb3c19ff-584d-419c-a72e-a314b0056b65';

    public static function any(): Result
    {
        return Result::create(
            Uuid::fromString(self::RESULT_TEST_ID),
            10,
            5
        );
    }
}