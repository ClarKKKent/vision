<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Repository;


use App\Domain\Core\Uuid;
use App\Domain\Core\UuidInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

abstract class AbstractMysqlRepository
{
    protected EntityManagerInterface $entityManager;

    protected ObjectRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository($this->getClass());
    }

    public function nextIdentity(): UuidInterface
    {
        return new Uuid(\Ramsey\Uuid\Uuid::uuid4()->toString());
    }

    abstract protected function getClass(): string;
}