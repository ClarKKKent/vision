<?php


namespace App\Infrastructure\Core\Persistence;


interface ReadModelInterface
{
    public function serialize(): array;
}