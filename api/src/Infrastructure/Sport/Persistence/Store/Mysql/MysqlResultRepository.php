<?php


namespace App\Infrastructure\Sport\Persistence\Store\Mysql;


use App\Domain\Core\Uuid;
use App\Domain\Core\UuidInterface;
use App\Domain\Sport\Entity\Result;
use App\Domain\Sport\Repository\ResultRepositoryInterface;
use App\Infrastructure\Core\Exception\ModelNotFoundException;
use App\Infrastructure\Core\Persistence\Doctrine\Repository\AbstractMysqlRepository;

final class MysqlResultRepository extends AbstractMysqlRepository implements ResultRepositoryInterface
{
    protected function getClass(): string
    {
        return Result::class;
    }

    public function nextIdentity(): Uuid
    {
        return Uuid::fromString(\Ramsey\Uuid\Uuid::uuid4());
    }

    public function save(Result $result): void
    {
        $this->entityManager->persist($result);
        $this->entityManager->flush();
    }

    public function delete(Result $result): void
    {
        $this->entityManager->remove($result);
        $this->entityManager->flush();
    }

    public function oneById(UuidInterface $id): Result
    {
        /** @var Result $result */
        if (!$result = $this->repository->find($id->toString())) {
            throw new ModelNotFoundException('Result not found.');
        }

        return $result;
    }
}