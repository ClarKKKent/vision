<?php


namespace App\Infrastructure\Sport\Persistence\ReadModel;


use App\Infrastructure\Core\Persistence\ReadModelInterface;

final class ExerciseView implements ReadModelInterface
{
    public ?string $id;

    public ?string $title;

    public ?string $description;

    public function serialize(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description
        ];
    }
}