<?php


namespace App\Application\Sport\Result\Query\GetResult;


use App\Application\Core\Item;
use App\Application\Core\QueryHandlerInterface;
use App\Infrastructure\Sport\Persistence\Query\ResultCollectionInterface;

final class GetResultQueryHandler implements QueryHandlerInterface
{
    private ResultCollectionInterface $resultCollection;

    public function __construct(ResultCollectionInterface $resultCollection)
    {
        $this->resultCollection = $resultCollection;
    }

    public function __invoke(GetResultQuery $query): Item
    {
        $result = $this->resultCollection->one($query);

        return new Item($result);
    }
}