<?php


namespace App\Application\Sport\Result\Query\GetResultsList;


use App\Application\Core\AbstractListQuery;
use App\Application\Core\QueryInterface;

final class GetResultsListQuery extends AbstractListQuery implements QueryInterface
{
    public string $userId;
}