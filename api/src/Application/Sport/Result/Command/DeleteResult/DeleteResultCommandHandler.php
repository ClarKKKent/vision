<?php


namespace App\Application\Sport\Result\Command\DeleteResult;


use App\Application\Core\CommandHandlerInterface;
use App\Domain\Core\Uuid;
use App\Domain\Sport\Repository\ResultRepositoryInterface;

final class DeleteResultCommandHandler implements CommandHandlerInterface
{
    private ResultRepositoryInterface $resultRepository;

    public function __construct(ResultRepositoryInterface $resultRepository)
    {
        $this->resultRepository = $resultRepository;
    }

    public function __invoke(DeleteResultCommand $message)
    {
        $result = $this->resultRepository->oneById(Uuid::fromString($message->resultId));

        $this->resultRepository->delete($result);
    }
}