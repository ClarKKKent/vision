<?php


namespace App\Application\Sport\Result\Command\CreateResult;


use App\Application\Core\CommandHandlerInterface;
use App\Application\Sport\Exercise\Command\CreateExercise\CreateExerciseCommand;
use App\Domain\Core\Uuid;
use App\Domain\Sport\Entity\Result;
use App\Domain\Sport\Repository\ExerciseRepositoryInterface;
use App\Domain\Sport\Repository\ResultRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Core\Security\IdentityFetcherInterface;

final class CreateResultCommandHandler implements CommandHandlerInterface
{
    private ResultRepositoryInterface $resultRepository;

    private ExerciseRepositoryInterface $exerciseRepository;

    private IdentityFetcherInterface $identityFetcher;

    private UserRepositoryInterface $userRepository;

    public function __construct(
        ResultRepositoryInterface $resultRepository,
        ExerciseRepositoryInterface $exerciseRepository,
        IdentityFetcherInterface $identityFetcher,
        UserRepositoryInterface $userRepository)
    {
        $this->exerciseRepository = $exerciseRepository;
        $this->resultRepository = $resultRepository;
        $this->identityFetcher = $identityFetcher;
        $this->userRepository = $userRepository;
    }

    public function __invoke(CreateResultCommand $message)
    {
        $result = Result::create(
            $this->resultRepository->nextIdentity(),
            $message->weight,
            $message->quantity
        );

        $user = $this->userRepository->oneByIdentity($this->identityFetcher->get());
        $user->applyResult($result);

        $exercise = $this->exerciseRepository->oneById(Uuid::fromString($message->exerciseId));
        $exercise->applyResult($result);

        $this->resultRepository->save($result);
    }
}