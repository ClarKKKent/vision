<?php


namespace App\Application\Sport\Exercise\Command\UpdateExercise;


use App\Application\Core\CommandInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class UpdateExerciseCommand implements CommandInterface
{
    public ?string $exerciseId;

    /**
     * @Assert\NotBlank
     * @Assert\Length(min=6)
     */
    public ?string $title;

    /**
     * @Assert\NotBlank
     * @Assert\Length(min=10)
     */
    public ?string $description;

    public function __construct(?string $exerciseId, ?string $title, ?string $description)
    {
        $this->exerciseId = $exerciseId;
        $this->title = $title;
        $this->description = $description;
    }

}