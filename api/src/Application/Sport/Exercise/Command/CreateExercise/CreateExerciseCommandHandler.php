<?php


namespace App\Application\Sport\Exercise\Command\CreateExercise;


use App\Application\Core\CommandHandlerInterface;
use App\Domain\Sport\Entity\Exercise;
use App\Domain\Sport\Repository\ExerciseRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Core\Security\IdentityFetcherInterface;

final class CreateExerciseCommandHandler implements CommandHandlerInterface
{
    private ExerciseRepositoryInterface $exerciseRepository;

    private IdentityFetcherInterface $identityFetcher;

    private UserRepositoryInterface $userRepository;

    public function __construct(
        ExerciseRepositoryInterface $exerciseRepository,
        IdentityFetcherInterface $identityFetcher,
        UserRepositoryInterface $userRepository)
    {
        $this->exerciseRepository = $exerciseRepository;
        $this->identityFetcher = $identityFetcher;
        $this->userRepository = $userRepository;
    }

    public function __invoke(CreateExerciseCommand $message): void
    {
        $exercise = Exercise::create(
            $this->exerciseRepository->nextIdentity(),
            $message->title,
            $message->description
        );

        $user = $this->userRepository->oneByIdentity($this->identityFetcher->get());
        $user->attachExercise($exercise);

        $this->exerciseRepository->save($exercise);
    }
}