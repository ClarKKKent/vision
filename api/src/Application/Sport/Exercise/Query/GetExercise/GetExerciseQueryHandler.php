<?php


namespace App\Application\Sport\Exercise\Query\GetExercise;


use App\Application\Core\Item;
use App\Application\Core\QueryHandlerInterface;
use App\Infrastructure\Sport\Persistence\Query\ExerciseCollectionInterface;

final class GetExerciseQueryHandler implements QueryHandlerInterface
{
    private ExerciseCollectionInterface $exerciseCollection;

    public function __construct(ExerciseCollectionInterface $exerciseCollection)
    {
        $this->exerciseCollection = $exerciseCollection;
    }

    public function __invoke(GetExerciseQuery $query): Item
    {
        $exercise = $this->exerciseCollection->one($query->id);

        return new Item($exercise);
    }
}