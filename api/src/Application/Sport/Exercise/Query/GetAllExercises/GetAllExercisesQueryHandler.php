<?php


namespace App\Application\Sport\Exercise\Query\GetAllExercises;


use App\Application\Core\Collection;
use App\Application\Core\QueryHandlerInterface;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Core\Security\IdentityFetcherInterface;
use App\Infrastructure\Sport\Persistence\Query\ExerciseCollectionInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;

final class GetAllExercisesQueryHandler implements QueryHandlerInterface
{
    public const EXERCISES_CACHE_KEY = 'exercises';

    private ExerciseCollectionInterface $exerciseCollection;

    private UserRepositoryInterface $userRepository;

    private IdentityFetcherInterface $identityFetcher;

    private AdapterInterface $cache;

    public function __construct(
        ExerciseCollectionInterface $exerciseCollection,
        UserRepositoryInterface $userRepository,
        IdentityFetcherInterface $identityFetcher,
        AdapterInterface $cache)
    {
        $this->exerciseCollection = $exerciseCollection;
        $this->userRepository = $userRepository;
        $this->identityFetcher = $identityFetcher;
        $this->cache = $cache;
    }

    public function __invoke(GetAllExercisesQuery $query): Collection
    {
        $item = $this->cache->getItem(self::EXERCISES_CACHE_KEY);

        if (!$item->isHit()) {
           $results = $this->executeDataFromDb($query);
           $item->set($results);
           $item->expiresAfter(new \DateInterval('PT60S'));
           $this->cache->save($item);

           return new Collection($results);
        }

        return new Collection($item->get());
    }

    private function executeDataFromDb(GetAllExercisesQuery $query): array
    {
        $user = $this->userRepository->oneByIdentity($this->identityFetcher->get());

        $query->userId = $user->getId()->toString();

        return $this->exerciseCollection->all($query);
    }

}