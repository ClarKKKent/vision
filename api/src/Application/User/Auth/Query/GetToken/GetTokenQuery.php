<?php


namespace App\Application\User\Auth\Query\GetToken;


use App\Application\Core\QueryInterface;

final class GetTokenQuery implements QueryInterface
{
    public ?string $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }
}