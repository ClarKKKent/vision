<?php


namespace App\Application\User\Auth\Query\GetToken;


use App\Application\Core\QueryHandlerInterface;
use App\Infrastructure\Core\Security\AuthenticationProviderInterface;
use App\Infrastructure\User\Persistence\Query\UserCollectionInterface;


final class GetTokenQueryHandler implements QueryHandlerInterface
{
    private UserCollectionInterface $userCollection;

    private AuthenticationProviderInterface $authProvider;

    public function __construct(UserCollectionInterface $userCollection, AuthenticationProviderInterface $authProvider)
    {
        $this->userCollection = $userCollection;
        $this->authProvider = $authProvider;
    }

    public function __invoke(GetTokenQuery $query)
    {
        $user = $this->userCollection->oneByEmailForAuth($query->email);

        return $this->authProvider->generateToken($user);
    }
}