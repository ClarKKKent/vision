<?php


namespace App\Application\User\Auth\Command\SigninUser;


use App\Application\Core\CommandHandlerInterface;
use App\Infrastructure\Core\Exception\AuthenticationException;
use App\Infrastructure\Core\Exception\ModelNotFoundException;
use App\Infrastructure\Core\Security\Password\PasswordHasherInterface;
use App\Infrastructure\User\Persistence\Query\UserCollectionInterface;

final class SigninUserCommandHandler implements CommandHandlerInterface
{
    private UserCollectionInterface $userCollection;

    private PasswordHasherInterface $passwordHasher;

    public function __construct(UserCollectionInterface $userCollection, PasswordHasherInterface $passwordHasher)
    {
        $this->userCollection = $userCollection;
        $this->passwordHasher = $passwordHasher;
    }

    public function __invoke(SigninUserCommand $message)
    {
        try {
            $user = $this->userCollection->oneByEmailForAuth($message->email);
        } catch (ModelNotFoundException $e) {
            throw new AuthenticationException();
        }

        if (!$this->passwordHasher->verify($message->password, $user->password_hash)) {
            throw new AuthenticationException();
        }
    }
}