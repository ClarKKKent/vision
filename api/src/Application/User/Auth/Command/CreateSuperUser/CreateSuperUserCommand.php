<?php

namespace App\Application\User\Auth\Command\CreateSuperUser;


use App\Application\Core\CommandInterface;

final class CreateSuperUserCommand implements CommandInterface
{
    public ?string $email;

    public ?string $password;

    public function __construct(?string $email, ?string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }
}