<?php


namespace App\Application\Core;


use App\Infrastructure\Core\Persistence\ReadModelInterface;

final class Item
{
    private array $resource;

    private ?ReadModelInterface $readModel;

    public function __construct(?ReadModelInterface $readModel)
    {
        $this->readModel = $readModel;
        $this->resource = $this->readModel->serialize();
    }

    public function resource(): array
    {
        return $this->resource;
    }
}