<?php


namespace App\UI\Cli\User;


use App\Application\User\Auth\Command\CreateSuperUser\CreateSuperUserCommand;
use App\Infrastructure\Core\Bus\CommandBusInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

final class CreateSuperUserConsoleCommand extends Command
{
    protected static $defaultName = 'app:create-superuser';

    private CommandBusInterface $commandBus;

    public function __construct(
        CommandBusInterface $commandBus,
        string $name = null)
    {
        parent::__construct($name);
        $this->commandBus = $commandBus;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        $questionEmail = new Question('Enter your email: ');
        $questionPassword = new Question('Enter your password: ');

        $this->commandBus->handle(
            new CreateSuperUserCommand(
                $helper->ask($input, $output, $questionEmail),
                $helper->ask($input, $output, $questionPassword)
            )
        );

        $output->writeln('SuperUser successfully created');

        return Command::SUCCESS;
    }
}