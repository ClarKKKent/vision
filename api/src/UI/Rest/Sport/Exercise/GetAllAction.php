<?php


namespace App\UI\Rest\Sport\Exercise;


use App\Application\Sport\Exercise\Query\GetAllExercises\GetAllExercisesQuery;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class GetAllAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/sport/exercises/all", methods={"GET"})
     * @return JsonResponse
     */
    public function __invoke(): JsonResponse
    {
        $exercises = $this->ask(
            new GetAllExercisesQuery()
        );

        return $this->okCollection($exercises);
    }
}