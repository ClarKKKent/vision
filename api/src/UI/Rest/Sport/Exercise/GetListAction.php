<?php


namespace App\UI\Rest\Sport\Exercise;


use App\Application\Sport\Exercise\Query\GetExercisesList\GetExercisesListQuery;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class GetListAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/sport/exercises", methods={"GET"})
     */
    public function __invoke(): JsonResponse
    {
        $exercises = $this->ask(new GetExercisesListQuery());

        return $this->okPaginatedCollection($exercises);
    }
}