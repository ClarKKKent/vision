<?php


namespace App\UI\Rest\Sport\Result;


use App\Application\Sport\Result\Query\GetResult\GetResultQuery;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class GetOneAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/sport/results/{id}", methods={"GET"})
     * @param string $id
     * @return JsonResponse
     */
    public function __invoke(string $id): JsonResponse
    {
        $result = $this->ask(
            new GetResultQuery($id)
        );

        return $this->ok($result);
    }
}