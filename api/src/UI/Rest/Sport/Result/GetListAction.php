<?php


namespace App\UI\Rest\Sport\Result;


use App\Application\Sport\Result\Query\GetResultsList\GetResultsListQuery;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class GetListAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/sport/results", methods={"GET"})
     * @return JsonResponse
     */
    public function __invoke(): JsonResponse
    {
        $results = $this->ask(
            new GetResultsListQuery()
        );

        return $this->okPaginatedCollection($results);
    }
}