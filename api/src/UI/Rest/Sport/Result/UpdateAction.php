<?php


namespace App\UI\Rest\Sport\Result;


use App\Application\Sport\Result\Command\UpdateResult\UpdateResultCommand;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class UpdateAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/sport/results/{id}", methods={"PUT"})
     * @param string $id
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(string $id, Request $request): JsonResponse
    {
        $command = new UpdateResultCommand(
            $id,
            $request->get('weight'),
            $request->get('quantity'),
            $request->get('exercise_id')
        );

        $this->exec($command);

        return $this->noContent();
    }
}