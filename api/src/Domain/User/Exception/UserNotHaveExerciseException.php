<?php


namespace App\Domain\User\Exception;


use Throwable;

class UserNotHaveExerciseException extends \LogicException
{
    public function __construct($message = "User don't have exercise.", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}