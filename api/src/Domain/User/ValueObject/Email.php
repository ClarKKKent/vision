<?php


namespace App\Domain\User\ValueObject;


use Webmozart\Assert\Assert;

class Email
{
    private string $value;

    private function __construct(string $value)
    {
        Assert::email($value);
        $this->value = $value;
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}