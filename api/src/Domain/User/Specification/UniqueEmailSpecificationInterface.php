<?php


namespace App\Domain\User\Specification;


interface UniqueEmailSpecificationInterface
{
    public function isSatisfiedBy($value): bool;
}