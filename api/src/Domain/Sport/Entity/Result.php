<?php


namespace App\Domain\Sport\Entity;


use App\Domain\Core\UuidInterface;
use App\Domain\User\Entity\User;

class Result
{
    private UuidInterface $id;

    private float $weight;

    private int $quantity;

    private \DateTimeImmutable $createdAt;

    private \DateTimeImmutable $updatedAt;

    private ?Exercise $exercise;

    private ?User $user;

    public function __construct(
        UuidInterface $id,
        float $weight,
        int $quantity)
    {
        $this->id = $id;
        $this->weight = $weight;
        $this->quantity = $quantity;
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->exercise = null;
        $this->user = null;
    }

    public static function create(
        UuidInterface $id,
        float $weight,
        int $quantity) :self
    {
        return new self(
            $id,
            $weight,
            $quantity
        );
    }

    public function update(
        float $weight,
        int $quantity): self
    {
        $this->weight = $weight;
        $this->quantity = $quantity;
        $this->updatedAt = new \DateTimeImmutable();

        return $this;
    }

    public function attachUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function applyExercise(Exercise $exercise): self
    {
        $this->exercise = $exercise;

        return $this;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getWeight(): float
    {
        return $this->weight;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getExercise(): ?Exercise
    {
        return $this->exercise;
    }
}