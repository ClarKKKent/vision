<?php


namespace App\Domain\Sport\Repository;


use App\Domain\Core\Uuid;
use App\Domain\Core\UuidInterface;
use App\Domain\Sport\Entity\Result;

interface ResultRepositoryInterface
{
    public function nextIdentity(): Uuid;

    public function save(Result $result): void;

    public function delete(Result $result): void;

    public function oneById(UuidInterface $id): Result;
}