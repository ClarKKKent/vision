<?php


namespace App\Domain\Core;


interface UuidInterface
{
    public function toString(): string;
}