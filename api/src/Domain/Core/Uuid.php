<?php


namespace App\Domain\Core;


use Webmozart\Assert\Assert;

class Uuid implements UuidInterface
{
    private string $value;

    public function __construct(string $value)
    {
        Assert::uuid($value);
        $this->value = $value;
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    public function toString(): string
    {
       return $this->value;
    }

    public function __toString()
    {
        return $this->value;
    }
}