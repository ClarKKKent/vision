<?php


namespace App\Tests\UI\Rest\Sport\Result;


use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Result\ResultsFixture;
use App\Tests\UI\UITestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class GetResultsListTest extends UITestCase
{
    private ?Client $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(new ResultsFixture());
        $this->executeFixtures();

        self::ensureKernelShutdown();

        $this->client = $this->createAuthenticatedClient();
    }

    public function testSuccess()
    {
        $this->client->request(
            'GET',
            '/api/v1/sport/results',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        self::assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
}