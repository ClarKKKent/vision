<?php


namespace App\Tests\UI\Rest\Sport\Result;


use App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Exercise\ExerciseBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExerciseFixture;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User\UserForAuthFixture;
use App\Tests\UI\UITestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class CreateResultTest extends UITestCase
{
    private ?Client $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(new UserForAuthFixture());
        $this->addFixture(new ExerciseFixture());
        $this->executeFixtures();

        self::ensureKernelShutdown();

        $this->client = $this->createAuthenticatedClient();
    }

    public function testSuccess()
    {
        $this->client->request(
            'POST',
            '/api/v1/sport/results',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'weight' => 15.0,
                'quantity' => 5,
                'exercise_id' => ExerciseBuilder::EXERCISE_TEST_ID
            ])
        );

        self::assertEquals(201, $this->client->getResponse()->getStatusCode());
    }


}