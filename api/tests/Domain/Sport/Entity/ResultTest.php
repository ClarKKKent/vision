<?php


namespace App\Tests\Domain\Sport\Entity;


use App\Domain\Core\Uuid;
use App\Domain\Sport\Entity\Result;
use PHPUnit\Framework\TestCase;

class ResultTest extends TestCase
{
    public function testSuccess()
    {
        $result = Result::create(
            $id = Uuid::fromString(\Ramsey\Uuid\Uuid::uuid4()),
            $weight = 18.5,
            $quantity = 5
        );

        self::assertEquals($id, $result->getId());
        self::assertEquals($weight, $result->getWeight());
        self::assertEquals($quantity, $result->getQuantity());
    }
}