<?php


namespace App\Tests\Domain\Sport\Entity;


use App\Domain\Core\Uuid;
use App\Domain\Sport\Entity\Exercise;
use PHPUnit\Framework\TestCase;

class ExerciseTest extends TestCase
{
    public function testSuccess()
    {
        $exercise = Exercise::create(
            $id = new Uuid(\Ramsey\Uuid\Uuid::uuid4()),
            $title = 'test',
            $description = 'test',
            $user = null
        );

        self::assertEquals($id, $exercise->getId());
        self::assertEquals($title, $exercise->getTitle());
        self::assertEquals($description, $exercise->getDescription());
        self::assertEquals($user, $exercise->getUser());
    }
}