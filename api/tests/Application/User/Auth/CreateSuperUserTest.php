<?php


namespace App\Tests\Application\User\Auth;


use App\Application\User\Auth\Command\CreateSuperUser\CreateSuperUserCommand;
use App\Tests\Application\ApplicationTestCase;

class CreateSuperUserTest extends ApplicationTestCase
{
    public function testSuccess()
    {
        $command = new CreateSuperUserCommand(
          'test@test.com',
          'password'
        );
        $this->exec($command);

        self::assertTrue(true);
    }
}